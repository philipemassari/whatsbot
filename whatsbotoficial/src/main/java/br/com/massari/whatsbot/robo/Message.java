package br.com.massari.whatsbot.robo;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import br.com.massari.whatsbot.robo.Keyboard;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import com.teamdev.jxbrowser.chromium.*;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiers;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiersBuilder;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import javax.swing.*;
import java.awt.*;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.*;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyEventType.*;
import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

//mport static java.awt.event.KeyEvent.*;

import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;

import br.com.massari.whatsbot.robo.Keyboard;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import com.teamdev.jxbrowser.chromium.*;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiers;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiersBuilder;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import javax.swing.*;
import java.awt.*;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.*;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyEventType.*;

/*
Classe responsavel por ler as mensaaens do chat.

As funcoes aqui listadas comente leem texto, imagens e audios
sao ignorados.

Se o usuario enviar uma imagem ou audio, isso quebra o loop que 
esta percorrendo as mensagens de texto, e ele interpreta como
ultima mensagem a ultima mensagem que foi enviada antes da imagem
ou audio, mesmo que depois da imagem ou audio contenha algum texto.
 */

public class Message {
	
	public Browser browser;
	public Chat chat;
	
	public Message(Browser bro, Chat cha){
		browser = bro;
		chat = cha;
	}
	
	public String getLastMessageFromTheNumber(String numero){
		try {
			//busca o chat na barra de busca
			chat.searchBar.findInSearchBar(numero);
			//se o chat for encontrado na lista
			if(chat.chatFoundOnTheListFind(numero)){
				//abra o chat
				chat.openChat(numero);
				Thread.sleep(500);
				//se ele estiver aberto
				//da um erro se ele precisa mandar varias mensagens no memso chat
				if(chat.chatIsOpened(numero) == true){
					return getLastMessageReceived();
				} else {
					//espere mais um pouco para tentar novamente
					Thread.sleep(500);
					//se ele estiver aberto 
					if(chat.chatIsOpened(numero) == true){
					return getLastMessageReceived();
					} else {
						System.out.println("Nao foi possivel abrir o chat no tempo requisitado, chat pulado.");
						return "CHAT_NOT_OPENED";
					}
				}
			} else {
				//se o numero nao foi encontrado na lista
				//procurar na barra de busca
				Thread.sleep(500);
				System.out.println("CHAT_NOT_FOUND");
				return "CHAT_NOT_OPENED";
			}			
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			return "MSG_NOT_FOUND";
		}
	}
		
	
	//classe... esse metodo precisa ser melhorado urgentemente.
	public String getLastMessageReceived(){
		browser.executeJavaScript("var x = document.querySelectorAll(" + "\"" + "div._3_7SH._3DFk6.message-in > div.Tkt2p > div.copyable-text >" +
		" div._3zb-j.ZhF0n > span.selectable-text.invisible-space.copyable-text" + "\");"); 
		browser.executeJavaScript("var aux;");
		browser.executeJavaScript("var i = -1;");
		browser.executeJavaScript("i=-1; while(aux != 'undefined'){ i++; aux = x[i].innerHTML; console.log(aux); if(aux == 'undefined'){break;}}");
		JSValue jsReturn = browser.executeJavaScriptAndReturnValue("x[i - 1].innerHTML;");
		try{
			String lastMessage = String.valueOf(jsReturn.getStringValue());
			return lastMessage;
		} catch (Exception e){
			System.out.println("MSG_NOT_FOUND");
			System.out.println(e);
			return "MESSAGE_NOT_FOUND";
		}
	}	
	
	public String getLastMessageSent(){
		browser.executeJavaScript("var x = document.querySelectorAll(" + "\"" + "div._3_7SH._3DFk6.message-out > div.Tkt2p > div.copyable-text >" +
		" div._3zb-j.ZhF0n > span.selectable-text.invisible-space.copyable-text" + "\");"); 
		browser.executeJavaScript("var aux;");
		browser.executeJavaScript("var i = -1");
		browser.executeJavaScript("i=-1; while(aux != 'undefined'){ i++; aux = x[i].innerHTML; console.log(aux); if(aux == 'undefined'){break;}}");
		JSValue jsReturn = browser.executeJavaScriptAndReturnValue("x[i - 1].innerHTML;");
		try{
			String lastMessage = String.valueOf(jsReturn.getStringValue());
			return lastMessage;
		} catch (Exception e){
			System.out.println("MSG_NOT_FOUND");
			System.out.println(e);
			return "MESSAGE_NOT_FOUND";
		}
	}	
}


