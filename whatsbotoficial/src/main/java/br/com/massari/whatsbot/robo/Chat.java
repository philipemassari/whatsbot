package br.com.massari.whatsbot.robo;
import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import br.com.massari.whatsbot.robo.Keyboard;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import com.teamdev.jxbrowser.chromium.*;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiers;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiersBuilder;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import javax.swing.*;
import java.awt.*;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.*;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyEventType.*;

/*
 Esta classe e responsavel por lidar com tudo que esteja relacionado ao chat, no caso a conversa.
 */
public class Chat {
	
	public Browser browser;
	public FindBar searchBar;
	
	 public Chat(Browser brow){
		browser = brow;
		searchBar = new FindBar(browser);
	}

	//abre o chat do numero passado como parametro
	public void openChat(String numero){
		browser.executeJavaScript("clickEvent.initEvent ('mousedown', true, true);" + 
            	"document.querySelector(" + "\"" + "span[title='" + numero + "']" + "\").dispatchEvent (clickEvent);");
	}
	
	//envia uma mensagem para o numero de telefone especificado
	public boolean sendMessageTo(String mensagem, String numero){
		try {
			//busca o chat na barra de busca
			searchBar.findInSearchBar(numero);
			//se o chat for encontrado na lista
			if(chatFoundOnTheListFind(numero)){
				//abra o chat
				openChat(numero);
				Thread.sleep(500);
				//se ele estiver aberto
				//da um erro se ele precisa mandar varias mensagens no memso chat
				if(chatIsOpened(numero) == true){
					if(messageBoxIsSelected()){
						Keyboard.typeText(browser, mensagem);
						Keyboard.sendText(browser);
						return true;
					} else {
						//espera um pouquinho e tenta de novo
						Thread.sleep(500);
						if(messageBoxIsSelected()){
							Keyboard.typeText(browser, mensagem);
							Keyboard.sendText(browser);
							return true;
						} else {
							System.out.println("Nao conseguiu selecionar a caixa de texto.");
							return false;
						}
					}
				} else {
					//espere mais um pouco para tentar novamente
					Thread.sleep(500);
					//se ele estiver aberto 
					if(chatIsOpened(numero) == true){
						if(messageBoxIsSelected()){
							Keyboard.typeText(browser, mensagem);
							Keyboard.sendText(browser);	
							return true;
						} else { 
							System.out.println("Nao conseguiu selecionar a caixa de texto no tempo requisitado.");
							return false;
						}
					} else {
						System.out.println("Nao foi possivel abrir o chat no tempo requisitado, chat pulado.");
						return false;
					}
				}
			} else {
				//se o numero nao foi encontrado na lista
				//procurar na barra de busca
				Thread.sleep(500);
				System.out.println("Chat nao encontrado na lista");
				return false;
			}			
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
			return false;
		}
	}
	
	//verifica se esta aberto o chat do numero especificado
	public boolean chatIsOpened(String number){
		JSValue jsReturn = browser.executeJavaScriptAndReturnValue("document.querySelector(" + "\"" + "div._2zCDG > span[title='" + number + "']" + "\").innerHTML;");	
		try{
			if(jsReturn.getStringValue().toString().equals(number)){
				System.out.println("O numero da conversa aberta corresponde! : " + jsReturn.getStringValue().toString());
				return true;
			} else {
				System.out.println("O numero da conversa aberta NAO corresponde!");
				return false;
			}
		} catch (Exception e){
			System.out.println("O numero da conversa aberta NAO corresponde!");
			System.out.println(e);
			return false;
		}
	}
	
	//verifica se uma conversa existe na lista
	public boolean chatFoundOnTheListFind(String number){
		JSValue jsReturn = browser.executeJavaScriptAndReturnValue("document.querySelector(" + "\"" + "span[title='" + number + "']" + " > span\").innerHTML;");
		try{
			if(jsReturn.getStringValue().toString().equals(number)){
				System.out.println("Chat encontrado na lista! : " + jsReturn.getStringValue().toString());
				return true;
			} else {
				System.out.println("Chat NAO encontrado na lista");
				return false;
			}
		} catch (Exception e){
			System.out.println("Chat NAO encontrado na lista");
			System.out.println(e);
			return false;
		}
	}
	
	public boolean messageBoxIsSelected(){
		try {
			JSValue jsReturn = browser.executeJavaScriptAndReturnValue("document.querySelector(" + "\"" + "div._3F6QL._2WovP.focused > div._39LWd"  + "\").innerHTML;");
			if(!jsReturn.equals("")){
				System.out.println("Caixa de texto selecionada : " + jsReturn.getStringValue().toString());
				return true;
			} else  {
				System.out.println("Caixa de mensagem nao selecionada");
				return false;
			}
			
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			System.out.println("Exception: Caixa de mensagem nao selecionada");
			return false;
		}
		
	}

}
