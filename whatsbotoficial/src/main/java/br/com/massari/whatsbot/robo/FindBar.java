package br.com.massari.whatsbot.robo;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import br.com.massari.whatsbot.robo.Keyboard;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import com.teamdev.jxbrowser.chromium.*;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiers;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiersBuilder;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import javax.swing.*;
import java.awt.*;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.*;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyEventType.*;

public class FindBar {
	
	public Browser browser;
	
	 public FindBar(Browser brow){
		browser = brow;
	}
	 
	 public void selectFindBar(){
			try {
				browser.executeJavaScript("clickEvent.initEvent ('mousedown', true, true);" + 
				"document.querySelector(" + "\"" + ".C28xL" + "\").dispatchEvent (clickEvent);");
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}
		
		public boolean findBarIsSelected(){
			try {
				JSValue jsReturn = browser.executeJavaScriptAndReturnValue("document.querySelector(" + "\"" + "div.gQzdc._3sdhb > button.C28xL" + "\").innerHTML;");
				if(!jsReturn.equals("")){
					return true;
				} else  {
					System.out.println("Barra nao selecionada");
					return false;
				}
				
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				System.out.println("Exception: Barra nao selecionada");
				return false;
			}
		}

		//seleciona a barra de busca e escreve nela o numero passado como parametro
		public void findInSearchBar(String number){
			try {
				//selecionar barra de busca
				selectFindBar();
				Thread.sleep(500);
				//se ele conseguir selecionar
				if(findBarIsSelected()){
					Keyboard.typeText(browser, number);
				} else {
					selectFindBar();
					Thread.sleep(500);
					Keyboard.typeText(browser, number);
				}
				Thread.sleep(1000);
				System.out.println("Busca finalizada");
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}
}
