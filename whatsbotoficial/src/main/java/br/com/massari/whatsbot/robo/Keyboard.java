package br.com.massari.whatsbot.robo;

import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_0;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_1;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_2;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_3;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_4;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_5;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_6;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_7;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_8;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_9;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_A;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_B;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_C;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_D;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_E;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_F;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_G;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_H;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_I;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_J;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_K;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_L;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_M;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_N;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_O;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_OEM_1;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_OEM_2;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_OEM_4;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_OEM_5;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_OEM_6;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_OEM_7;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_OEM_8;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_OEM_COMMA;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_OEM_MINUS;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_OEM_PERIOD;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_OEM_PLUS;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_P;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_Q;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_R;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_RETURN;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_S;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_SPACE;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_T;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_U;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_V;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_W;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_X;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_Y;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.VK_Z;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyEventType.PRESSED;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyEventType.RELEASED;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyEventType.TYPED;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiers;

/*
 Esta classe e responsavel pelos eventos de digitacao, escrever as mensagens e
 as enviar, tudo que envolve o teclado.
 */

public class Keyboard {
	
	public static void forwardKeyEvent(Browser browser, KeyCode code, char character) {
        browser.forwardKeyEvent(new BrowserKeyEvent(PRESSED, code, character));
        browser.forwardKeyEvent(new BrowserKeyEvent(TYPED, code, character));
        browser.forwardKeyEvent(new BrowserKeyEvent(RELEASED, code, character));
    }

    public static void forwardKeyEvent(Browser browser, KeyCode code) {
        browser.forwardKeyEvent(new BrowserKeyEvent(PRESSED, code));
        browser.forwardKeyEvent(new BrowserKeyEvent(TYPED, code));
        browser.forwardKeyEvent(new BrowserKeyEvent(RELEASED, code));
    }

    public static void forwardKeyEvent(Browser browser, KeyCode code, KeyModifiers modifiers) {
        browser.forwardKeyEvent(new BrowserKeyEvent(PRESSED, code, modifiers));
        browser.forwardKeyEvent(new BrowserKeyEvent(TYPED, code, modifiers));
        browser.forwardKeyEvent(new BrowserKeyEvent(RELEASED, code, modifiers));
    }
    
  //pressiona a tecla enter
  	public static void sendText(Browser browser){
  		Keyboard.forwardKeyEvent(browser, VK_RETURN);   
  	}
  		
  	//escreve em qualquer lugar que estiver selecionado
  	public static void typeText(Browser browser, String texto){ 		
  		char[] character = texto.toCharArray();
  		for (int i = 0; i < character.length; i++) {			
  			try{        			
  				switch (character[i]) {	
  				case 'á': Keyboard.forwardKeyEvent(browser, VK_A, 'á'); break;
  				case 'í': Keyboard.forwardKeyEvent(browser, VK_I, 'í'); break;
  				case 'ó': Keyboard.forwardKeyEvent(browser, VK_O, 'ó'); break;
  				case 'ú': Keyboard.forwardKeyEvent(browser, VK_U, 'ú'); break;
  				case 'é': Keyboard.forwardKeyEvent(browser, VK_E, 'é'); break;		
  				case 'â': Keyboard.forwardKeyEvent(browser, VK_A, 'â'); break;
  				case 'ê': Keyboard.forwardKeyEvent(browser, VK_E, 'ê'); break;
  				case 'î': Keyboard.forwardKeyEvent(browser, VK_I, 'î'); break;
  				case 'ô': Keyboard.forwardKeyEvent(browser, VK_O, 'ô'); break;			
  				case 'à': Keyboard.forwardKeyEvent(browser, VK_A, 'à'); break;
  				case 'ì': Keyboard.forwardKeyEvent(browser, VK_I, 'ì'); break;
  				case 'ò': Keyboard.forwardKeyEvent(browser, VK_O, 'ò'); break;
  				case 'ù': Keyboard.forwardKeyEvent(browser, VK_U, 'ù'); break;
  				case 'è': Keyboard.forwardKeyEvent(browser, VK_E, 'è'); break;        			
  				case 'ü': Keyboard.forwardKeyEvent(browser, VK_U, 'ü'); break;	        				        				        			
  				case 'ã': Keyboard.forwardKeyEvent(browser, VK_A, 'ã'); break;
  				case 'õ': Keyboard.forwardKeyEvent(browser, VK_O, 'õ'); break;
  				case 'ç': Keyboard.forwardKeyEvent(browser, VK_C, 'ç'); break; 			       			
  		        case 'a': Keyboard.forwardKeyEvent(browser, VK_A, 'a'); break;
  		        case 'b': Keyboard.forwardKeyEvent(browser, VK_B, 'b'); break;
  		        case 'c': Keyboard.forwardKeyEvent(browser, VK_C,'c'); break;
  		        case 'd': Keyboard.forwardKeyEvent(browser, VK_D,'d'); break;
  		        case 'e': Keyboard.forwardKeyEvent(browser, VK_E,'e'); break;
  		        case 'f': Keyboard.forwardKeyEvent(browser, VK_F,'f'); break;
  		        case 'g': Keyboard.forwardKeyEvent(browser, VK_G,'g'); break;
  		        case 'h': Keyboard.forwardKeyEvent(browser, VK_H,'h'); break;
  		        case 'i': Keyboard.forwardKeyEvent(browser, VK_I,'i'); break;
  		        case 'j': Keyboard.forwardKeyEvent(browser, VK_J,'j'); break;
  		        case 'k': Keyboard.forwardKeyEvent(browser, VK_K,'k'); break;
  		        case 'l': Keyboard.forwardKeyEvent(browser, VK_L,'l'); break;
  		        case 'm': Keyboard.forwardKeyEvent(browser, VK_M,'m'); break;
  		        case 'n': Keyboard.forwardKeyEvent(browser, VK_N,'n'); break;
  		        case 'o': Keyboard.forwardKeyEvent(browser, VK_O,'o'); break;
  		        case 'p': Keyboard.forwardKeyEvent(browser, VK_P,'p'); break;
  		        case 'q': Keyboard.forwardKeyEvent(browser, VK_Q,'q'); break;
  		        case 'r': Keyboard.forwardKeyEvent(browser, VK_R,'r'); break;
  		        case 's': Keyboard.forwardKeyEvent(browser, VK_S,'s'); break;
  		        case 't': Keyboard.forwardKeyEvent(browser, VK_T,'t'); break;
  		        case 'u': Keyboard.forwardKeyEvent(browser, VK_U,'u'); break;
  		        case 'v': Keyboard.forwardKeyEvent(browser, VK_V,'v'); break;
  		        case 'w': Keyboard.forwardKeyEvent(browser, VK_W,'w'); break;
  		        case 'x': Keyboard.forwardKeyEvent(browser, VK_X,'x'); break;
  		        case 'y': Keyboard.forwardKeyEvent(browser, VK_Y,'y'); break;
  		        case 'z': Keyboard.forwardKeyEvent(browser, VK_Z,'z'); break;
  		        case 'A': Keyboard.forwardKeyEvent(browser, VK_A, 'A'); break;
  		        case 'B': Keyboard.forwardKeyEvent(browser, VK_B,'B'); break;
  		        case 'C': Keyboard.forwardKeyEvent(browser, VK_C,'C'); break;
  		        case 'D': Keyboard.forwardKeyEvent(browser, VK_D,'D'); break;
  		        case 'E': Keyboard.forwardKeyEvent(browser, VK_E,'E'); break;
  		        case 'F': Keyboard.forwardKeyEvent(browser, VK_F,'F'); break;
  		        case 'G': Keyboard.forwardKeyEvent(browser, VK_G,'G'); break;
  		        case 'H': Keyboard.forwardKeyEvent(browser, VK_H,'H'); break;
  		        case 'I': Keyboard.forwardKeyEvent(browser, VK_I,'I'); break;
  		        case 'J': Keyboard.forwardKeyEvent(browser, VK_J,'J'); break;
  		        case 'K': Keyboard.forwardKeyEvent(browser, VK_K,'K'); break;
  		        case 'L': Keyboard.forwardKeyEvent(browser, VK_L,'L'); break;
  		        case 'M': Keyboard.forwardKeyEvent(browser, VK_M,'M'); break;
  		        case 'N': Keyboard.forwardKeyEvent(browser, VK_N,'N'); break;
  		        case 'O': Keyboard.forwardKeyEvent(browser, VK_O,'O'); break;
  		        case 'P': Keyboard.forwardKeyEvent(browser, VK_P,'P'); break;
  		        case 'Q': Keyboard.forwardKeyEvent(browser, VK_Q,'Q'); break;
  		        case 'R': Keyboard.forwardKeyEvent(browser, VK_R,'R'); break;
  		        case 'S': Keyboard.forwardKeyEvent(browser, VK_S,'S'); break;
  		        case 'T': Keyboard.forwardKeyEvent(browser, VK_T,'T'); break;
  		        case 'U': Keyboard.forwardKeyEvent(browser, VK_U,'U'); break;
  		        case 'V': Keyboard.forwardKeyEvent(browser, VK_V,'V'); break;
  		        case 'W': Keyboard.forwardKeyEvent(browser, VK_W,'W'); break;
  		        case 'X': Keyboard.forwardKeyEvent(browser, VK_X,'X'); break;
  		        case 'Y': Keyboard.forwardKeyEvent(browser, VK_Y,'Y'); break;
  		        case 'Z': Keyboard.forwardKeyEvent(browser, VK_Z,'Z'); break;
  		        case '0': Keyboard.forwardKeyEvent(browser, VK_0,'0'); break;
  		        case '1': Keyboard.forwardKeyEvent(browser, VK_1,'1'); break;
  		        case '2': Keyboard.forwardKeyEvent(browser, VK_2,'2'); break;
  		        case '3': Keyboard.forwardKeyEvent(browser, VK_3,'3'); break;
  		        case '4': Keyboard.forwardKeyEvent(browser, VK_4,'4'); break;
  		        case '5': Keyboard.forwardKeyEvent(browser, VK_5,'5'); break;
  		        case '6': Keyboard.forwardKeyEvent(browser, VK_6,'6'); break;
  		        case '7': Keyboard.forwardKeyEvent(browser, VK_7,'7'); break;
  		        case '8': Keyboard.forwardKeyEvent(browser, VK_8,'8'); break;
  		        case '9': Keyboard.forwardKeyEvent(browser, VK_9,'9'); break;	        	   	        	    
  		        case '-': Keyboard.forwardKeyEvent(browser, VK_OEM_MINUS,'-'); break;
  		        case '.': Keyboard.forwardKeyEvent(browser, VK_OEM_PERIOD,'.'); break;
  		        case ',': Keyboard.forwardKeyEvent(browser, VK_OEM_COMMA,','); break;	        	    
  		        case ':': Keyboard.forwardKeyEvent(browser, VK_OEM_1,':'); break;
  		        case ';': Keyboard.forwardKeyEvent(browser, VK_OEM_1,';'); break;	        	        
  		        case '?': Keyboard.forwardKeyEvent(browser, VK_OEM_2,'?'); break;
  		        case '/': Keyboard.forwardKeyEvent(browser, VK_OEM_2,'/'); break;        	        
  		        case '{': Keyboard.forwardKeyEvent(browser, VK_OEM_4,'{'); break;
  		        case '[': Keyboard.forwardKeyEvent(browser, VK_OEM_4,'['); break;	        	        
  		        case '|': Keyboard.forwardKeyEvent(browser, VK_OEM_5,'|'); break;	        	      
  		        case '}': Keyboard.forwardKeyEvent(browser, VK_OEM_6,'}'); break;
  		        case ']': Keyboard.forwardKeyEvent(browser, VK_OEM_6,']'); break;	        	        
  		        case '"': Keyboard.forwardKeyEvent(browser, VK_OEM_7,'"'); break;        	        
  		        case '!': Keyboard.forwardKeyEvent(browser, VK_OEM_8,'!'); break;
  		        case '$': Keyboard.forwardKeyEvent(browser, VK_OEM_8,'$'); break;	        	        
  		        case '+': Keyboard.forwardKeyEvent(browser, VK_OEM_PLUS,'+'); break;
  		        case '=': Keyboard.forwardKeyEvent(browser, VK_OEM_PLUS,'='); break;        	      	        	
  		        case ' ': Keyboard.forwardKeyEvent(browser, VK_SPACE, ' '); break;
  		        default:
  		            throw new IllegalArgumentException("Cannot type character " + character);
  		        }    		
  			} catch(Exception b) {
  				System.out.println("caractere nao encontrado");
  				break;
  			}      			
  		}// fim do for
  		
  		//ABAIXO COMENTADO ESTA O METODO DE INSERIR O TEXTO E TENTAR SIMULAR O KEYDOWN, AINDA NAO FUNCIONAL
  		//browser.executeJavaScript("document.querySelector('._2S1VP.copyable-text.selectable-text').innerHTML = '" + texto + "';");		
  		////browser.executeJavaScript("document.querySelector('.weEq5').dispatchEvent (clickEvent);");
  		//Teclado.forwardKeyEvent(browser, VK_RETURN);
  	}

}
