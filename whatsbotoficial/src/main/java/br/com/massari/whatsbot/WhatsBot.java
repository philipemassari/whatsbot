package br.com.massari.whatsbot;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

//mport static java.awt.event.KeyEvent.*;

import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.events.ConsoleEvent;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;

import br.com.massari.whatsbot.robo.Chat;
import br.com.massari.whatsbot.robo.Keyboard;
import br.com.massari.whatsbot.robo.Message;
import br.com.massari.whatsbot.robo.Utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import com.teamdev.jxbrowser.chromium.*;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiers;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiersBuilder;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import javax.swing.*;
import java.awt.*;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.*;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyEventType.*;

public class WhatsBot {
	
	private Browser browser = null;
	private BrowserView view = null;
	public static final String url = "https://web.whatsapp.com/";
	private static final int EXIT_ON_CLOSE = 0;
	Chat chat;
	Utils utils;
	Message message;
	
	//apenas inicia o browser
	public WhatsBot(){
	   browser = new Browser();
	   chat = new Chat(browser);
	   message = new Message(browser, chat);
	   utils = new Utils(chat, message);
	   	   
	   browser.addLoadListener(new LoadAdapter() {
            @Override
            public void onFinishLoadingFrame(FinishLoadingEvent event) {
                if (event.isMainFrame()) {
                	//carrega no js todas as variaveis necessarias para o funcionamento do chatbot
                	loadVariables();                                                 
                }
            }    
        }); 
	    this.browser.loadURL(url);	   
	}

	//carrega a interface grafica
	public void loadView(){
		this.view = new BrowserView(this.browser);
		JFrame frame = new JFrame();
	    frame.add(this.view, BorderLayout.CENTER);
	    frame.setSize(700, 500);
	    frame.setVisible(true);	 
	    Utils.testar(browser, view);
	}
	
	//carrega no js todas as variaveis necessarias para o funcionamento do chatbot
	public void loadVariables(){
		//criacao da variavel que permite o funcionamento do evento mouseDown em todo chatbot, nao apagar.
    	browser.executeJavaScript("var clickEvent = document.createEvent ('MouseEvents');");
	}
	
}
