package br.com.massari.whatsbot.robo;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

//mport static java.awt.event.KeyEvent.*;

import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import br.com.massari.whatsbot.robo.Keyboard;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import com.teamdev.jxbrowser.chromium.*;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiers;
import com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyModifiersBuilder;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import javax.swing.*;
import java.awt.*;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyCode.*;
import static com.teamdev.jxbrowser.chromium.BrowserKeyEvent.KeyEventType.*;
/*
 Esta classe contem metodos que podem ser uteis em diversos lugares da aplicacao, como escrever
 mensagem, enviar, usar barra de busca e etc.
 */

public class Utils {
	
	public static Chat chat;
	public static Message message;
	
	public Utils(Chat cha, Message messag){
		chat = cha;
		message = messag;
	}

	//executa qualquer teste escrito depois de pressionar a tecla ESC
	public static void testar(Browser browser, BrowserView view){
		//destinado a testes:
	    view.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {		
            	if(e.getKeyCode() == KeyEvent.VK_ESCAPE){    		
            		try {
//            			chat.sendMessageTo("teste chatbot msg", "Davi Salles");    
            			System.out.println("Ultima mensagem recebida: " +message.getLastMessageReceived());
//            			System.out.print(message.getLastMessageFromTheNumber("Batalia"));
            			System.out.println("Ultima mensagem enviada: " + message.getLastMessageSent());
            		} catch (Exception e2) {
            			// TODO Auto-generated catch block
            			e2.printStackTrace();
            		}
            	 }
            }
        });
	}
	
	public static String getSecureMessage(String message){
		message.replaceAll("'", "");
		message.replaceAll("<", "");
		message.replaceAll(">", "");
		message.replaceAll("<script>", "");
		message.replaceAll("\"", "");
		message.replaceAll("\\", "");
		message.replaceAll("//", "");
		message.replaceAll("=", "");
		message.replaceAll("\0", "\\0");
		message.replaceAll("\n", "\\n");
		message.replaceAll("\r", "\\r");
		message.replaceAll("\"", "\\\"");
		message.replaceAll("\\x1a", "\\Z");
		return message;
	}
	
	
}
